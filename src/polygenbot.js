var Bot = require('./bot');

var InMemoryStore = require('./store/inmemory');
var MongoDBStore = require('./store/mongodb');

module.exports = {
  Bot: Bot,

  InMemoryStore: InMemoryStore,
  MongoDBStore: MongoDBStore
};
