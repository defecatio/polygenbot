var _ = require('lodash');
var uuid = require('node-uuid');
var fs = require('fs');
var os = require('os');
var path = require('path');
var mime = require('mime');
var Promise = require('promise');
var request = require('request');
var validateJs = require('validate.js');
var momentParser = require('moment-parser');
var moment = require('moment');
var TelegramBot = require('node-telegram-bot-api');
var GoogleImages = require('google-images');

var Polygen = require('./polygen');
var InMemoryStore = require('./store/inmemory');

function Bot(conf) {

  var bot = this;
  var telegramBot;
  var googleImagesClient;

  // Initialize config
  var defaultConfig = {
    store: new InMemoryStore(),
    spamEvery: '2 hours'
  };
  var config = _.extend({}, defaultConfig, conf);

  // Validate config
  validateConfig(config);

  // Init Polygen engine
  var polygen = new Polygen();

  // Init Google Images client
  if (config.googleCseKey && config.googleApiKey) {
    googleImagesClient = GoogleImages(config.googleCseKey, config.googleApiKey);
  }

  // Start bot
  this.run = function () {
    telegramBot = new TelegramBot(config.telegramBotKey, { polling: true });

    telegramBot.on('message', function (msg) {
      ensureChat(msg.chat).then(function (chatState) {
        var text = msg.text;

        if (text) {

          if (text.match('/' + config.botName)) {
            return sendPolygen(chatState);
          }

          var match = text.match(/\/parla (.+)/);
          if (match) {
            if (match[1].startsWith('start')) {
              return config.store.findAll().then(function (chats) {
                var chatList = _.map(chats, function (chat) {
                  return '/parla_' + chat._id + ' ' + chat.name;
                });
                var txt = 'Scegli con chi vuoi far parlare il bot:\n' + chatList.join('\n');
                telegramBot.sendMessage(msg.chat.id, txt);
              });
            } else if (match[1].startsWith('stop')) {
              chatState.chatWith = null;
              config.store.save(chatState);
              telegramBot.sendMessage(msg.chat.id, 'Modalità parla disattivata!');
              return;
            }
          }

          match = text.match(/\/parla_(.+)/);
          if (match) {
            try {
              var parsedId = parseInt(match[1]);
              config.store.findOne(parsedId).then(function (chatWithChat) {
                if (!chatWithChat) {
                  return telegramBot.sendMessage(msg.chat.id, 'Errore: il destinatario non esiste!');
                }
                chatState.chatWith = match[1];
                config.store.save(chatState);
                return telegramBot.sendMessage(msg.chat.id, 'Modalità parla attivata: qualunque cosa scriverai d\'ora in poi sarà inoltrata a: ' + chatWithChat.name + '!\nPer interrompere la modalità, digita /parla stop');
              });
            } catch(ex) {
              return telegramBot.sendMessage(msg.chat.id, 'Errore: il destinatario non è valido!');
            }
          }
        }

        // Check if forwarding mode enabled
        if (chatState.chatWith) {
          if (msg.photo) {
            return telegramBot.sendPhoto(chatState.chatWith, msg.photo[0].file_id, { caption: msg.caption });
          }
          if (msg.document) {
            return telegramBot.sendVideo(chatState.chatWith, msg.document.file_id, { caption: msg.caption });
          }
          if (msg.audio) {
            return telegramBot.sendAudio(chatState.chatWith, msg.audio.file_id);
          }
          if (msg.voice) {
            return telegramBot.sendVoice(chatState.chatWith, msg.voice.file_id);
          }

          return telegramBot.sendMessage(chatState.chatWith, msg.text);
        }

        // Check if disturbing message should be sent
        eventuallySendPolygen(chatState);
      });
    });
  };

  this.runPolygen = function () {
    return polygen.run(config.polygenGrammarFile)
      .then(function (quote) {
        var quoteObj = {
          body: quote,
          pictureKeyword: null
        };
        // Detect picture directive
        var match = quote.match(/\[PICTURE\](.*?)\[\/PICTURE\]/);
        if (match) {
          quoteObj.pictureKeyword = match[1];
          // Cleanup picture directive
          quoteObj.body = quote.replace(match[0], '');
        }
        return quoteObj;
      });
  };

  function getChatName(chat) {
    if (chat.title) return chat.title;
    if (chat.first_name) return chat.first_name + ' ' + chat.last_name;
    if (chat.username) return chat.username;
  }

  function ensureChat(chat) {
    return config.store.findOne(chat.id)
      .then(function (loadedChat) {
        if (!loadedChat) {
          loadedChat = { _id: chat.id, name: getChatName(chat) };
          return config.store.save(chat.id, loadedChat).then(function() {
            return loadedChat;
          });
        } else {
          return loadedChat;
        }
      });
  }

  function sendPicture(chatState, keyword) {

    if (!googleImagesClient) {
      return Promise.reject('No Google API Key configured');
    }

    return googleImagesClient.search(keyword, { page: getRandomFromArray([1, 2, 3]) })
      .then(function (imageResults) {
        // Pick random image
        var imageResult = getRandomFromArray(imageResults);
        // Download image
        var requestParams = {
          url: imageResult.url,
          encoding: null
        };
        return Promise.denodeify(request)(requestParams);
      })
      .then(function (image) {
        // Generate image filename
        var imageFilename = 'polygenbot_' + config.botName + '_' + uuid.v4();
        var imagePath = os.tmpdir() + path.sep + imageFilename + '.' + mime.extension(image.headers['content-type']);
        // Store image to disk
        return Promise.denodeify(fs.writeFile)(imagePath, image.body)
          .then(function () {
            return {
              fileName: imageFilename,
              path: imagePath
            };
          });
      })
      .then(function (image) {
        // Send image to bot
        return telegramBot.sendPhoto(chatState._id, image.path, { file_id: image.fileName });
      });
  }

  function eventuallySendPolygen(chatState) {

    if (!config.spamEvery) {
      return;
    }

    // Check if can send message
    if(chatState.lastTimePolygen) {
      var lastTimePolygen = moment(chatState.lastTimePolygen);
      if (lastTimePolygen.add(config.spamEvery).diff(moment()) > 0) {
        return;
      }
    }

    chatState.lastTimePolygen = moment().toJSON();
    config.store.save(chatState).then(function () {
      return sendPolygen(chatState)
        .catch(function (err) {
          console.error('Unable to send polygen message', err);
          chatState.lastTimePolygen = lastTimePolygen.toJSON();
          return config.store.save(chatState);
        });
    });
  }

  function sendPolygen(chatState) {

    telegramBot.sendChatAction(chatState._id, 'typing');
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        // Generate quote
        bot.runPolygen()
          .then(function (quoteObj) {
            // Send message
            return telegramBot.sendMessage(chatState._id, quoteObj.body)
              .then(function () {
                // Check if need to send picture
                if (quoteObj.pictureKeyword) {
                  sendPicture(chatState, quoteObj.pictureKeyword).catch(function (e) {
                    console.error(e);
                  });
                }
              });
          })
          .then(resolve)
          .catch(reject);
      }, 10000);
    });
  }

  function getRandomFromArray(items) {
    return items[getRandomNumber(0, items.length - 1)];
  }

  function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function validateConfig(config) {
    var constraints = {
      botName: {
        presence: true,
        format: {
          pattern: "[a-z0-9]+",
          flags: "i",
          message: "can only contain a-z and 0-9"
        }
      },
      telegramBotKey: {
        presence: true
      },
      polygenGrammarFile: {
        presence: true
      },
      spamEvery: {
        presence: false,
        naturalLanguageDate: {
          type: "MomentDuration"
        }
      }
    };

    // Config validation routines
    validateJs.validators.naturalLanguageDate = function (value, options, key, attributes) {
      try {
        var parsedDate = momentParser.parse(config.spamEvery);
        if (parsedDate.type !== options.type) {
          return 'must be a ' + options.type;
        }
      } catch (ex) {
        return 'is not a valid natural language date/duration: ' + ex.message;
      }
    };

    var validationResult = validateJs.validate(config, constraints);

    if (validationResult) {
      var printableValidationResult = _.map(validationResult, function (value, key) {
        return '\t' + key + ' -> ' + '\t' + value.join('\n\t\t\t');
      }).join('\n');
      throw new Error('Config error:\n' + printableValidationResult);
    }

    if (config.spamEvery) {
      var parsedSpamEvery = momentParser.parse(config.spamEvery);
      config.spamEvery = moment.duration(parsedSpamEvery.value, parsedSpamEvery.unit);
    }
  }
}

module.exports = Bot;