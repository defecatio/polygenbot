var _ = require('lodash');
var Promise = require('promise');
var MongoClient = require('mongodb').MongoClient;

var MongoDBStore = function (url) {

    var collectionName = 'chats';

    var client;

    this.findOne = function (key) {
      return ensureClient()
        .then(function (coll) {
          return Promise.denodeify(_.bind(coll.find, coll))({ _id: key });
        })
        .then(function (cursor) {
          return cursor.limit(1).next();
        });
    };

    this.findAll = function () {

      var nextLoop = function (cursor, acc) {
          if(!acc) {
              acc = [];
          }
          return Promise.denodeify(_.bind(cursor.next, cursor))()
              .then(function(value) {
                  if(!value) {
                      return acc;
                  }
                  acc.push(value);
                  return nextLoop(cursor, acc);
              });
      };

      return ensureClient()
        .then(function (coll) {
          return Promise.denodeify(_.bind(coll.find, coll))();
        })
        .then(nextLoop);
    };

    this.save = function (key, value) {

      if (_.isObject(key)) {
        value = key;
        key = key._id;
      }

      return ensureClient()
        .then(function (coll) {
          return Promise.denodeify(_.bind(coll.updateOne, coll))({ _id: key }, { '$set': value}, { upsert: true });
        });
    };

    function ensureClient() {
      if (!client) {
        client = Promise.denodeify(MongoClient.connect)(url)
          .then(function (db) {
            return db.collection(collectionName);
          })
          .catch(function (err) {
            client = null;
            throw err;
          });
      }
      return client;
    }
  };

module.exports = MongoDBStore;