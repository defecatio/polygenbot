var Promise = require('promise');

var InMemoryStore = function() {

  var store = {};

  this.findOne = function(key) {
    return new Promise(function (resolve, reject) {
      resolve(store[key]);
    });
  };

  this.findAll = function() {
    return new Promise(function (resolve, reject) {
      resolve(store);
    });
  };

  this.save = function(key, value) {
    if(_.isObject(key)) {
      value = key;
      key = key._id;
    }
    return new Promise(function (resolve, reject) {
      store[key] = value;
      resolve(store[key]);
    });
  };
};

module.exports = InMemoryStore;