var path = require('path');
var exec = require("child_process").exec;
var Promise = require('promise');
var _ = require('lodash');
var HtmlEntities = require('html-entities').AllHtmlEntities;

var Polygen = function (conf) {

  var defaultConfig = {
    bin: __dirname + path.sep + '../bin/polygen'
  };

  var config = _.extend({}, defaultConfig, conf);

  this.run = function (grammarFile) {
    return Promise.denodeify(exec)(config.bin + ' ' + grammarFile)
      .then(function (rawQuote) {
        // Convert html entities in UTF-8
        var htmlEntities = new HtmlEntities();
        return htmlEntities.decode(rawQuote);
      });
  }
};

module.exports = Polygen;